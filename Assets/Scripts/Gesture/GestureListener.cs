﻿using UnityEngine;
using System.Collections;

public interface IGestureListener {
    
    // Respond to tap events
    void OnTapEvent(Vector2 tapLocation);
    
    // Respond to flick events
    void OnFlickEvent(Vector2 flickDirection, float flickMagnitude);

    // Respond to hold events
    void OnHoldEvent(Vector2 holdLocation);
}
