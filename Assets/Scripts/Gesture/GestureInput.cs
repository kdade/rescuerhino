﻿using UnityEngine;
using System.Collections;

public class GestureInput : MonoBehaviour, IPausable {
	
    // Tuning parameters
	public int NumTapFrames = 8;
    public int NumFlickFrames = 3;
    public int NumHoldFrames = 15;
	public int TapEnergyThreshold = 30;

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }
	
	// Instance vars ---------------------------------------- //
	// These are the gesture state vars
	private int touchFrame;
	private Vector2 touchDirection;
	private float touchEnergy;
    bool detectingGesture;
    // Keep a list of all listeners
    private ArrayList gestureListeners;
	
	// Use this for initialization
	void Awake() {
        // Initialize array list of listeners
        gestureListeners = new ArrayList();
        // Initialize gesture measurement variables
		touchFrame = 0;
		touchDirection = Vector2.zero;
		touchEnergy = 0.0f;
        detectingGesture = false;
	}

    // For registering
    void Start() {
        // Register pausable
        GameController.GC.RegisterPausable(this);
    }
	
	// Update is called once per frame
	void Update() {
        // Check for pause
        if (isPaused) return;

		// Get the first touch
		if (Input.touchCount > 0) {
			// We only use the first touch
			Touch touch = Input.GetTouch (0);
			// Count the number of frames in the touch.
			touchFrame = (touch.phase==TouchPhase.Began) ? 0 : touchFrame + 1;
			// Initialize on gesture begin
            if (touch.phase == TouchPhase.Began) {
                touchDirection = Vector2.zero;
                touchEnergy = 0.0f;
                detectingGesture = true;
            }
            // Otherwise keep track of variables
            else {
                touchDirection = touch.deltaPosition;
                touchEnergy += touch.deltaPosition.magnitude;
            }
            if (detectingGesture) {
                // Detect tap gesture
                if (touch.phase == TouchPhase.Ended && touchFrame < NumTapFrames && touchEnergy < TapEnergyThreshold) {
                    foreach (IGestureListener listener in gestureListeners) {
                        listener.OnTapEvent(touch.position);
                    }
                    detectingGesture = false;
                }
                // Detect flick gesture
                else if ((touch.phase == TouchPhase.Ended || touchFrame >= NumFlickFrames) && touchEnergy >= TapEnergyThreshold) {
                    foreach (IGestureListener listener in gestureListeners) {
                        listener.OnFlickEvent(touchDirection.normalized, touchDirection.magnitude);
                    }
                    detectingGesture = false;
                }
                // Detect hold gesture
                else if (touchFrame >= NumHoldFrames && touchEnergy < TapEnergyThreshold) {
                    foreach (IGestureListener listener in gestureListeners) {

                        listener.OnHoldEvent(touch.position);
                    }
                    detectingGesture = false;
                }
            }
		}
	}

    // Register a new IGestureListener
    public void RegisterGestureListener(IGestureListener listener) {
        gestureListeners.Add(listener);
    }

    // Unregister IGestureListener
    public void UnregisterGestureListener(IGestureListener listener) {
        gestureListeners.Remove(listener);
    }
}