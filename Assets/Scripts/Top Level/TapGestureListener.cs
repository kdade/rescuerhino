﻿using UnityEngine;
using System.Collections;

public class TapGestureListener : MonoBehaviour, IGestureListener {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTapEvent(Vector2 tapLocation) {
        if (Physics.Raycast(new Vector3(tapLocation.x, tapLocation.y, 10.0f), new Vector3(0.0f, 0.0f, -1.0f), 100.0f)) {
            // Call different game controller objects based on hit
            RaycastHit hit;
            Physics.Raycast(new Vector3(tapLocation.x, tapLocation.y, 10.0f), new Vector3(0.0f, 0.0f, -1.0f), out hit);
            if (hit.collider != null) {
                AudioMaster.AM.PlayBalloonNormalPop();
                // Cases for each button type
                if (hit.collider.gameObject.tag == "MenuStartGame") {
                    GameController.GC.StartNewLevel(0);
                }
                else if (hit.collider.gameObject.tag == "MenuSettings") {
                    GameController.GC.Settings();
                }
                else if (hit.collider.gameObject.tag == "MenuHowToPlay") {
                    GameController.GC.Rules();
                }
            }
        }
    }



    // Ignore
    public void OnHoldEvent(Vector2 holdLocation) {
       
    }

    public void OnFlickEvent(Vector2 flickDirection, float flickMagnitude) {

    }
}
