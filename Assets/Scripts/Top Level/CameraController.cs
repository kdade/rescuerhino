﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour, IPausable {

	public GameObject Rhino;
	public Vector2 Limits;
	public Vector2 Deadzone;
	public float TrackingAlpha;
	// Private
	private float camPosx;
	private float camPosy;
    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }
    // Visible game objects
    private ArrayList visibleObjects;

    // At beginning
    void Start() {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Intialize
        visibleObjects = new ArrayList();
    }

    // Collect
    void OnTriggerEnter2D(Collider2D other) {
        visibleObjects.Add(other.gameObject);
    }

    // Uncollect
    void OnTriggerExit2D(Collider2D other) {
        visibleObjects.Remove(other.gameObject);
    }

    // Check if visible
    public bool IsObjectVisible(GameObject obj) {
        return visibleObjects.Contains(obj);
    }

	// Update is called once per frame
	void Update() {
        // Check for pause
        if (isPaused) return;

		// Track the rhino
		float desPosx = Rhino.transform.position.x;
		float desPosy = Rhino.transform.position.y;
		// Deadzone
		if(Mathf.Abs(desPosx - transform.position.x) < Deadzone.x) {
			desPosx = camPosx;
		}
		if(Mathf.Abs (desPosy - transform.position.y) < Deadzone.y) {
			desPosy = camPosy;
		}
		// Smooth
		camPosx = TrackingAlpha * camPosx + (1 - TrackingAlpha) * desPosx;
		camPosy = TrackingAlpha * camPosy + (1 - TrackingAlpha) * desPosy;
		// Clamp to limits
		camPosx = Mathf.Clamp (camPosx,-Limits.x, Limits.x);
		camPosy = Mathf.Clamp (camPosy, -Limits.y, Limits.y);
		transform.position = new Vector3(camPosx, camPosy, transform.position.z);
	}
}
