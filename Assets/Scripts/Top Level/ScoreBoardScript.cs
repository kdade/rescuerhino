﻿using UnityEngine;
using System.Collections;

public class ScoreBoardScript : MonoBehaviour, IPausable {

    // Duration
    public float AnimationDuration = 1.0f;
    public float BaseSpriteScale = 0.4f;

    // Reference to the number sprites
    public GameObject sprite0;
    public GameObject sprite1;
    public GameObject sprite2;
    public GameObject sprite3;
    public GameObject sprite4;
    public GameObject sprite5;
    public GameObject sprite6;
    public GameObject sprite7;
    public GameObject sprite8;
    public GameObject sprite9;

    // These are the "radii" of the number sprites
    public float r0;
    public float r1;
    public float r2;
    public float r3;
    public float r4;
    public float r5;
    public float r6;
    public float r7;
    public float r8;
    public float r9;

    // Keep track of sprites
    private ArrayList sprites;
    // Animation
    private float animTime;

    //Pausable interface
    bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

    // Start
    void Start() {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Initialize
        sprites = new ArrayList();
    }

    // Animate sprites
    void Update() {
        // Pausable
        if (isPaused) return;
        // Animate
        Color color = Color.white;
        float scale = BaseSpriteScale;
        if (animTime < AnimationDuration) {
            color = 0.5f * (Color.white + Color.Lerp(Color.white, Color.yellow, Mathf.Sin(Mathf.PI * animTime / AnimationDuration)));
            scale = (1.0f + 0.3f * Mathf.Sin(Mathf.PI * animTime / AnimationDuration)) * BaseSpriteScale;
        }
        foreach (GameObject sprite in sprites) {
            sprite.GetComponentInChildren<SpriteRenderer>().color = color;
            sprite.transform.localScale = new Vector3(scale, scale, 1.0f);
        }
        // Increment
        animTime += Time.deltaTime;
    }

    // Set score
    public void SetScore(int score) {
        // Remove all previous sprites
        foreach (GameObject sprite in sprites) Destroy(sprite);
        sprites.Clear();
        // Instantiate new sprites (with animation?)
        int[] digits = GetSixLeastSignificantDigits(score);
        // Display digits
        DisplayDigits(digits);
        // Kick off digit animation
        animTime = 0.0f;
    }

    // Display digits
    private void DisplayDigits(int[] digits) {
        // Loop through left to right - ignore leading zeros
        Vector3 spritePos = transform.position;
        bool isLeading = true;
        for (int i = 0; i < 6; i++ ) {
            // Ignore leading zeros
            if (isLeading && i != 6 && digits[i] == 0) continue;
            // Otherwise spawn the appropriate sprite
            GameObject digitSprite;
            float radius;
            switch (digits[i]) {
                case 1: digitSprite = sprite1; radius = r1; break;
                case 2: digitSprite = sprite2; radius = r2; break;
                case 3: digitSprite = sprite3; radius = r3; break;
                case 4: digitSprite = sprite4; radius = r4; break;
                case 5: digitSprite = sprite5; radius = r5; break;
                case 6: digitSprite = sprite6; radius = r6; break;
                case 7: digitSprite = sprite7; radius = r7; break;
                case 8: digitSprite = sprite8; radius = r8; break;
                case 9: digitSprite = sprite9; radius = r9; break;
                default: digitSprite = sprite0; radius = r0; break;
            }
            // Move
            spritePos += radius * Vector3.right;
            // Place object
            GameObject sprite = Instantiate(digitSprite, spritePos, Quaternion.identity) as GameObject;
            sprite.transform.parent = transform;
            sprites.Add(sprite);
            // Move again
            spritePos += radius * Vector3.right;
            // Hit a digit and not leading anymore
            isLeading = false;
        }
    }

    // Returns an array of the six least significant digits
    private int[] GetSixLeastSignificantDigits(int number) {
        // Get number of digits
        int[] digits = new int[6];
        // Get the digits - this is simple enough to just hard code - LEFT TO RIGHT - use integer division
        digits[0] = (number / 100000) % 10;
        digits[1] = (number / 10000) % 10;
        digits[2] = (number / 1000) % 10;
        digits[3] = (number / 100) % 10;
        digits[4] = (number / 10) % 10;
        digits[5] = number % 10;
        // Done
        return digits;
    }
}
