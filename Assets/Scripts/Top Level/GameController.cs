﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    // Singleton
    public static GameController GC;

    // Store reference to the pause panel
    public GameObject PausePanel;
    public GameObject GameOverPanel;
    public GameObject LevelClearPanel;
    public ScoreBoardScript ScoreBoard;
    public int score1 = 100;
    public int score2 = 150;
    public int score3 = 200;

    // To turn on and off
    public Image musicbirds;
    public Image soundbirds;

    public bool musicOn = true;
    public bool soundsOn = true;

    public int difficulty = 1;

    ///////////////////////////////// THESE ARE NON PERSISTENT VARIABLES ////////////////////////////
    // All pausable objects
    private ArrayList pausables;
    // Perfect popping spree
    private int perfectSpree;
    // Score
    private int score;
    /////////////////////////////////////////////////////////////////////////////////////////////////
    bool loadingScene;
    string sceneName;
    float t;
    public int numBalloons = 12;
    private bool gameOver = false;
    

    // Singleton management
    void Awake() {
        if (GC != null) {
            musicOn = GameController.GC.musicOn;
            soundsOn = GameController.GC.soundsOn;
            score = GameController.GC.score;
            difficulty = GameController.GC.difficulty + 1;
            GameObject.Destroy(GC);
            numBalloons = 12;
        }
        GC = this;
        DontDestroyOnLoad(this);
        // For storing pausable objects
        pausables = new ArrayList();
        perfectSpree = 0;
    }

    void Update() {
        if (loadingScene) {
            t += Time.deltaTime;
        }
        if (t > 1.0f) {
            Application.LoadLevel(sceneName);
        }
        if (!loadingScene && numBalloons == 0) {
            LevelClear();
        }
    }

    // Get spree
    public int GetPerfectSpree() {
        return perfectSpree;
    }
    // Perfect pop
    public void RecordPerfectPop() {
        numBalloons--;
        perfectSpree++;
        switch (perfectSpree) {
            case 0: // fall through
            case 1: IncrementScore(score1);  break;
            case 2: IncrementScore(score2);  break;
            default: IncrementScore(score3); break;
        }
    }
    // Normal pop
    public void RecordNormalPop() {
        numBalloons--;
        perfectSpree = 0;
        IncrementScore(score1);
    }
    // Reset perfect spree
    public void RecordHeadbuttMiss() {
        perfectSpree = 0;
    }

    // Navigate to menu scene
    public void Menu() {
		if (PausePanel != null) {
			UnpauseGame ();
		}
        t = 0.0f;
        loadingScene = true;
        sceneName = "MenuScene";
        AudioMaster.AM.FadeOutMusic();
        AudioMaster.AM.PlayBalloonNormalPop();
    }

    // Navigate to the game scene
    public void StartNewLevel(int startingScore) {
        t = 0.0f;
        loadingScene = true;
        sceneName = "GameScene";
        difficulty = 1;
        AudioMaster.AM.FadeOutMusic();
        AudioMaster.AM.PlayBalloonNormalPop();
    }

	//Navigate to Settings scene
	public void Settings(){
        t = 0.0f;
        loadingScene = true;
        sceneName = "SettingsScene";
        AudioMaster.AM.FadeOutMusic();
        AudioMaster.AM.PlayBalloonNormalPop();
	}

	//Navigate to Rules scene
	public void Rules(){
         t = 0.0f;
        loadingScene = true;
        sceneName = "RulesScene";
        AudioMaster.AM.FadeOutMusic();
        AudioMaster.AM.PlayBalloonNormalPop();
	}

    // Music on/off
    public void ToggleMusic() {
        musicOn = !musicOn;
        musicbirds.enabled = musicOn;
    }
    // Sounds
    public void ToggleSounds() {
        soundsOn = !soundsOn;
        soundbirds.enabled = soundsOn;
    }
    // Game over
    public void GameOver() {
        if (gameOver) return;
        score = 0;
        difficulty = 1;
        AudioMaster.AM.PlayGameOver();
        GameOverPanel.SetActive(true);
        gameOver = true;
    }

    public void LevelClear() {
        if (gameOver) return;
        AudioMaster.AM.PlayLevelClear();
        PoacherAI.AI.RhinoIsCaught();
        LevelClearPanel.SetActive(true);
        gameOver = true;
    }

    // Pull up the hold menu
    public void PauseGame() {
        if (gameOver) return;
        foreach (IPausable pausable in pausables) {
                pausable.Pause();
        }
        if (PausePanel == null) Debug.Log("Didn't find pause panel");
        PausePanel.SetActive(true);
    }

    // Unpause game
    public void UnpauseGame() {
        foreach (IPausable pausable in pausables) {
            pausable.Unpause();
        }
        PausePanel.SetActive(false);
    }

    // Register pausable objects
    public void RegisterPausable(IPausable pausable) {
        pausables.Add(pausable);
    }





    ////////////////////////////////////////////////////////////////////////////////
    // Helpers //

    // Increment the score
    private void IncrementScore(int amount) {
        score += amount;
        ScoreBoard.SetScore(score);
    }

}
