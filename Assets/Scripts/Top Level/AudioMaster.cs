﻿using UnityEngine;
using System.Collections;

public class AudioMaster : MonoBehaviour, IPausable {

    // These are the audio clips
    // Oneshots
    public AudioClip jeepBeepOneshot;
    public AudioClip balloonNormalPopOneshot;
    public AudioClip balloonPerfectPopOneshot;
    public AudioClip rhinoGruntOneshot;
    public AudioClip rhinoSquealOneshot;
    public AudioClip rhinoBumpOneshot;
    public AudioClip poacherYell_1;
    public AudioClip poacherYell_2;
    public AudioClip poacherYell_3;
    public AudioClip netSwishOneshot;
    public AudioClip birdCawOneshot_1;
    public AudioClip birdCawOneshot_2;
    public AudioClip birdCawOneshot_3;
    public AudioClip gameOverOneshot;
    public AudioClip levelClearOneshot;

    // Pause Interface
    private bool isPaused;
    public void Pause() { isPaused = true; PauseAudio(); }
    public void Unpause() { isPaused = false; UnpauseAudio(); }

    // Singleton
    public static AudioMaster AM;

    /////////////////////////////////////////////////////////////////
    // These are the audio sources
    private AudioSource MusicSource;
    private AudioSource RhinoRunningLoopSource;
    private AudioSource RhinoSwimmingLoopSource;
    private AudioSource JeepLoopSource;
    private AudioSource BirdsLoopSource;
    private AudioSource OneshotsSource;
    // These are the state variables
    private bool MusicLoopOn;
    private bool RhinoRunningLoopOn;
    private bool RhinoSwimmingLoopOn;
    private bool JeepLoopOn;
    private bool BirdsLoopOn;
    private bool OneshotsOn;

    // State
    private int birdCawCount;
    private int poacherYellCount;
    private float timeSinceBirdCaw;

    // Fade time 
    private float fadeTime;
    private bool isFading;

	// Use this for initialization
	void Start () {
        // Singleton
        if (AM != null) {
            GameObject.Destroy(AM);
        }
        AM = this;
        // Register pausable
        GameController.GC.RegisterPausable(this);
	    // You heard the man!
        GetAudioSources();  
        // Initialize
        MusicLoopOn = GameController.GC.musicOn;
        MusicSource.enabled = GameController.GC.musicOn;

        RhinoRunningLoopSource.Stop();
        RhinoRunningLoopOn = true;
        

        RhinoSwimmingLoopSource.Stop();
        RhinoSwimmingLoopOn = false;
        
        
        JeepLoopSource.Stop();
        JeepLoopOn = false;

        BirdsLoopSource.Stop();
        BirdsLoopOn = false;

        OneshotsOn = GameController.GC.soundsOn;
        OneshotsSource.enabled = GameController.GC.soundsOn;

        // State
        birdCawCount = 0;
        poacherYellCount = 0;
        timeSinceBirdCaw = float.MaxValue;
	}

    // Time
    void Update() {
        MusicSource.enabled = GameController.GC.musicOn;
        RhinoRunningLoopSource.enabled = GameController.GC.soundsOn;
        RhinoSwimmingLoopSource.enabled = GameController.GC.soundsOn;
        JeepLoopSource.enabled = GameController.GC.soundsOn;
        BirdsLoopSource.enabled = GameController.GC.soundsOn;
        OneshotsSource.enabled = GameController.GC.soundsOn;
        if (isPaused) return;
        if (isFading) {
            if (fadeTime < 2.0f) MusicSource.volume = MusicSource.volume * 0.95f;
            else MusicSource.volume = 0.0f;
        }
        timeSinceBirdCaw += Time.deltaTime;
        fadeTime += Time.deltaTime;
    }

    ///////////////////////// Interface ////////////////////////////

    // Fade out music
    public void FadeOutMusic() {
        isFading = true;
        fadeTime = 0.0f;
    }

    // Idle noise
    public void RhinoIdleSound() {
        RhinoRunningLoopOn = false;
        RhinoRunningLoopSource.Stop();
        RhinoSwimmingLoopOn = false;
        RhinoSwimmingLoopSource.Stop();
    }
    // Running noise
    public void RhinoRunningSound() {
        if (!RhinoRunningLoopOn) {
            RhinoRunningLoopOn = true;
            RhinoRunningLoopSource.Play();
        }
        RhinoSwimmingLoopOn = false;
        RhinoSwimmingLoopSource.Stop();
    }
    // Swimming noise
    public void RhinoSwimmingSound() {
        if (!RhinoSwimmingLoopOn) {
            RhinoSwimmingLoopOn = true;
            RhinoSwimmingLoopSource.Play();
        }
        RhinoRunningLoopOn = false;
        RhinoRunningLoopSource.Stop();
    }
    // Jeep engine noise
    public void JeepEngineSoundVolume(float vol) {
        if (!JeepLoopOn) {
            JeepLoopOn = true;
            JeepLoopSource.Play();
        }
        JeepLoopSource.volume = vol;
    }
    public void JeepEngineSoundOff() {
        JeepLoopOn = false;
        JeepLoopSource.Stop();
    }
    // Bird flocking noise
    public void BirdFlockingSoundVolume(float vol) {
        if (!BirdsLoopOn && vol > 0.01f) {
            BirdsLoopOn = true;
            BirdsLoopSource.Play();
        }
        BirdsLoopSource.volume = Mathf.Min(BirdsLoopSource.volume, vol);
    }
    public void BirdFlockingSoundOff() {
        if (BirdsLoopSource.volume < 0.01f) {
            BirdsLoopOn = false;
            BirdsLoopSource.Stop();
        }
    }
    // Normal pop
    public void PlayBalloonNormalPop() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(balloonNormalPopOneshot);
    }
    // Perfect pop
    public void PlayBalloonPerfectPop() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(balloonPerfectPopOneshot); 
    }
    // Bump
    public void PlayRhinoBonk() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(rhinoBumpOneshot); 
    }
    // Grunt
    public void PlayRhinoGrunt() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(rhinoGruntOneshot); 
    }
    // Squeal
    public void PlayRhinoSqueal(float delay) {
        if (!OneshotsSource.enabled) return;
        StartCoroutine(WaitAndPlay(delay, rhinoSquealOneshot));
    }
    // Poacher throw
    public void PlayPoacherThrow() {
        if (!OneshotsSource.enabled) return;
        switch (poacherYellCount % 3) {
            case 0: OneshotsSource.PlayOneShot(poacherYell_1); break;
            case 1: OneshotsSource.PlayOneShot(poacherYell_2); break;
            case 2: OneshotsSource.PlayOneShot(poacherYell_3); break;
        }
        
        poacherYellCount++;
    }
    // Net swish 
    public void PlayNetSwish() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(netSwishOneshot); 
    }
    // Jeep beep
    public void PlayJeepBeep() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(jeepBeepOneshot);
    }
    // Bird caw
    public void PlayBirdCaw() {
        if (!OneshotsSource.enabled) return;
        if (timeSinceBirdCaw > 0.3f) {
            switch (birdCawCount % 3) {
                case 0: OneshotsSource.PlayOneShot(birdCawOneshot_1, 0.3f); break;
                case 1: OneshotsSource.PlayOneShot(birdCawOneshot_2, 0.3f); break;
                case 2: OneshotsSource.PlayOneShot(birdCawOneshot_3, 0.3f); break;
            }
            timeSinceBirdCaw = 0.0f;
        }
    }

    // Gameover sounds
    public void PlayGameOver() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(gameOverOneshot);
    }
    public void PlayLevelClear() {
        if (!OneshotsSource.enabled) return;
        OneshotsSource.PlayOneShot(levelClearOneshot);
    }

    ///////////////////////// Helpers /////////////////////////////

    // Get the attached audio sources
    private void GetAudioSources() {
        AudioSource[] sources = GetComponents<AudioSource>();
        if (sources.Length != 6) {
            Debug.Log("There are not 6 sources attached!!");
            return;
        }
        // Assign
        MusicSource = sources[0];
        RhinoRunningLoopSource = sources[1];
        RhinoSwimmingLoopSource = sources[2];
        JeepLoopSource = sources[3];
        BirdsLoopSource = sources[4];
        OneshotsSource = sources[5];
    }

    // Pause Audio
    private void PauseAudio() {
        MusicSource.Stop();
        RhinoRunningLoopSource.Stop();
        RhinoSwimmingLoopSource.Stop();
        JeepLoopSource.Stop();
        BirdsLoopSource.Stop();
        OneshotsSource.enabled = false; ;
    }

    // Unpause Audio
    private void UnpauseAudio() {
        if (!MusicLoopOn) MusicSource.Stop(); else MusicSource.Play();
        if (!RhinoRunningLoopOn) RhinoRunningLoopSource.Stop(); else RhinoRunningLoopSource.Play();
        if (!RhinoSwimmingLoopOn) RhinoSwimmingLoopSource.Stop(); else RhinoSwimmingLoopSource.Play();
        if (!JeepLoopOn) JeepLoopSource.Stop(); else JeepLoopSource.Play();
        if (!BirdsLoopOn) BirdsLoopSource.Stop(); else BirdsLoopSource.Play();
        OneshotsSource.enabled = OneshotsOn;
    }

    // Play audio after a delay
    private IEnumerator WaitAndPlay(float waitTime, AudioClip clip) {
        yield return new WaitForSeconds(waitTime);
        if (OneshotsSource.enabled) OneshotsSource.PlayOneShot(clip);

    }

    
}
