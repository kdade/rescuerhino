﻿using UnityEngine;
using System.Collections;

public class LevelCreatorScript : MonoBehaviour {

    // Grid resolution
    public int nx;
    public int ny;

    // Number of balloon
    public int NumBalloons = 12;

    // Reference to background sprites
    public SpriteRenderer backgroundSR;
    public Sprite background_0;
    public Sprite background_1;
    public Sprite background_2;

    // Reference to all prefabs
    public GameObject poacher;
    public GameObject balloon;
    public GameObject tree_0;
    public GameObject tree_1;
    public GameObject tree_2;
    public GameObject rock_0;
    public GameObject rock_1;
    public GameObject rock_2;
    public GameObject rock_3;
    public GameObject rock_4;
    public GameObject lake_0;
    public GameObject lake_1;
    public GameObject lake_2;

    // Lake radius
    public int MaxLakes = 3;
    public float LakeRadius = 25.0f;
    public float LakeProbability = 0.4f;
    private int whichLake;

    // Obstacle radius
    public int MaxObstacles = 6;
    public float ObstacleRadius = 10.0f;
    public float ObstacleProbability = 0.5f;

    // Safety radius for rhino at start
    public float rhinoStartRadius = 4.0f;
    public float poacherRadius = 5.0f;

    public int nballoons = 0;

    Vector3[,]spawnLocations;

    // Lakes
    private ArrayList lakes;
    // Obstacles
    private ArrayList obstacles;
    // Balloons
    private ArrayList balloons;
    // Poachers
    private ArrayList poachers;

	// Use this for initialization
	void Start () {
        int difficulty = PoacherAI.AI.DifficultyLevel;
        // State
        whichLake = Mathf.FloorToInt(Random.Range(0.0f, 2.99f));
        // Carve up the map
        CarveUpMap();
        // Assign a random background.
        ChooseBackground();
	    // Put in lakes
        MakeLakes(difficulty);
        // Make the obstacles
        MakeObstacles(difficulty);
        // Drop the balloons
        balloons = new ArrayList();
        DropObjects(balloon, false, NumBalloons, rhinoStartRadius, 5000, balloons, false);
        // Drop the poachers
        poachers = new ArrayList();
        DropObjects(poacher, true, difficulty + 1, poacherRadius, 5000, poachers, true);
        // Enable all
        EnableAllObjects();
	}

    // Hopefully this doesn't make collider issues
    private void EnableAllObjects() {
        foreach (GameObject poacher in poachers) poacher.SetActive(true);
        foreach (GameObject balloon in balloons) { balloon.SetActive(true); nballoons++; }
        foreach (GameObject lake in lakes) lake.SetActive(true);
        foreach (GameObject obstacle in obstacles) obstacle.SetActive(true);
    }

    // Calculate all of the spawn location
    private void CarveUpMap() {
        Vector2 limits = PoacherAI.AI.PlayableAreaLimits;
        spawnLocations = new Vector3[nx, ny];
        float xres = 2.0f * limits.x / (float)(nx);
        float yres = 2.0f * limits.y / (float)(ny);
        // Make all of the points
        for (int x = 0; x < nx; x++) {
            for (int y = 0; y < ny; y++) {
                spawnLocations[x,y] = new Vector3( -limits.x + (0.5f + x) * xres, -limits.y + (0.5f + y) * yres, 0.0f);
            }
        }
    }

    // Make up to three lakes
    private void MakeLakes(int difficulty) {
        // Initialize 
        lakes = new ArrayList();
        int numLakes = 0;
        for(int i = 0; i < MaxLakes; i++) if (Random.value < LakeProbability) numLakes++;
        int watchdog = 0;
        while(numLakes > 0) {
            // If this is taking to long, give up
            if (watchdog > 50) return;
            // Choose a random spawn location
            int ix = Mathf.FloorToInt(Random.Range(0.0f, nx - 0.01f));
            int iy = Mathf.FloorToInt(Random.Range(0.0f, ny - 0.01f));
            // Check versus all other lakes
            bool isOk = true;
            foreach (GameObject lake in lakes) {
                if (Vector3.Distance(spawnLocations[ix, iy], lake.transform.position) < 2 * LakeRadius) isOk = false;
            }
            // If location is ok, spawn with random 2D rotation
            if (isOk && !ViolatesRhinoStartRadius(spawnLocations[ix,iy], LakeRadius)) {
                Quaternion rot = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f));
                lakes.Add(Instantiate(NextLake(), spawnLocations[ix, iy], rot) as GameObject);
                numLakes--;
            }
            // Just in case
            watchdog++;
        }
    }

    // Make the balloons
    private void DropObjects(GameObject obj, bool rotationIsRandom, int number, float radius, int watchdoglimit, ArrayList compareList, bool avoidLakes) {
        int n = number;
        int watchdog = 0;
        while (n > 0 && watchdog < watchdoglimit) {
            // Choose a random spawn location
            int ix = Mathf.FloorToInt(Random.Range(0.0f, nx - 0.01f));
            int iy = Mathf.FloorToInt(Random.Range(0.0f, ny - 0.01f));
            // Check versus all other obstacles
            bool isOk = true;
            foreach (GameObject obstacle in obstacles) {
                if (Vector3.Distance(spawnLocations[ix, iy], obstacle.transform.position) < ObstacleRadius) isOk = false;
            }
            foreach (GameObject compare in compareList) {
                if (Vector3.Distance(spawnLocations[ix, iy], compare.transform.position) < radius) isOk = false;
            }
            if (avoidLakes) {
                foreach (GameObject lake in lakes) {
                    if (Vector3.Distance(spawnLocations[ix, iy], lake.transform.position) < LakeRadius) isOk = false;
                }
            }
            // If location is ok, spawn with random 2D rotation
            if (isOk && !ViolatesRhinoStartRadius(spawnLocations[ix, iy], radius)) {
                Quaternion rot = rotationIsRandom ? Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)) : Quaternion.identity;
                obstacles.Add(Instantiate(obj, spawnLocations[ix, iy], rot) as GameObject);
                n--;
            }
            watchdog++;
        }
    }

    // Make up to six obstacles
    private void MakeObstacles(int difficulty) {
        obstacles = new ArrayList();
        int numObstacles = 0;
        for (int i = 0; i < MaxObstacles; i++) if (Random.value < ObstacleProbability) numObstacles++;
        int watchdog = 0;
        while (numObstacles > 0) {
            // If this is take too long, give up
            if (watchdog > 1000) return;
            // Choose a random spawn location
            int ix = Mathf.FloorToInt(Random.Range(0.0f, nx - 0.01f));
            int iy = Mathf.FloorToInt(Random.Range(0.0f, ny - 0.01f));
            // Check versus all other obstacles
            bool isOk = true;
            foreach (GameObject obstacle in obstacles) {
                if (Vector3.Distance(spawnLocations[ix, iy], obstacle.transform.position) < ObstacleRadius) isOk = false;
            }
            // If location is ok, spawn with random 2D rotation
            if (isOk && !ViolatesRhinoStartRadius(spawnLocations[ix,iy], ObstacleRadius + rhinoStartRadius)) {
                Quaternion rot = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f));
                obstacles.Add(Instantiate(RandomObstacle(), spawnLocations[ix, iy], rot) as GameObject);
                numObstacles--;
            }
            // Just in case
            watchdog++;
        }
    }

    // Choose a random background
    private void ChooseBackground() {
        float val = Random.value;
        if (val < 0.333f) backgroundSR.sprite = background_0;
        else if (val < 0.666f) backgroundSR.sprite = background_1;
        else backgroundSR.sprite = background_2;
    }

    // Random obstacle
    private GameObject RandomObstacle() {
        float val = Random.value;
        if (val < 0.14f) return rock_0;
        if (val < 0.28f) return rock_1;
        if (val < 0.42f) return rock_2;
        if (val < 0.56f) return rock_3;
        if (val < 0.70) return tree_0;
        if (val < 0.84f) return tree_1;
        return tree_2;
    }

    // Random lake
    private GameObject NextLake() {
        whichLake++;
        if (whichLake % 3 == 0) return lake_0;
        if (whichLake % 3 == 1) return lake_1;
        return lake_2;
    }

    // Check bounds
    private bool IsInBounds(Vector3 position, float radius) {
        if (position.x > PoacherAI.AI.PlayableAreaLimits.x - radius) return false;
        if (position.x < -PoacherAI.AI.PlayableAreaLimits.x + radius) return false;
        if (position.y > PoacherAI.AI.PlayableAreaLimits.y - radius) return false;
        if (position.y < -PoacherAI.AI.PlayableAreaLimits.y + radius) return false;
        return true;
    }

    // Check rhino start radius
    private bool ViolatesRhinoStartRadius(Vector3 position, float radius) {
        if (position.magnitude < radius) return true;
        return false;
    }

    // Random 2D rotation.
}
