﻿using UnityEngine;
using System.Collections;

public interface IPausable {

    // Can be paused and unpaused
    void Pause();
    void Unpause();

}
