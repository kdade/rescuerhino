﻿using UnityEngine;
using System.Collections;

public class ObstacleColliderScript : MonoBehaviour, IPausable {
    // Obstacle collider is the Poachers smaller "vicinity" collider
    // Keep track of obstacles, balloons, enemies, traps.

    // All obstacles in view
    private ArrayList obstaclesInView;
    // All enemies in view
    private ArrayList enemiesInView;
    // All balloons in view
    private ArrayList balloonsInView;
    // Reference to the parent controller
    private PoacherController poacher;
    // Time since last forced redirect
    private float timeSinceObstacleRedirect;
    private float timeSinceEnemyRedirect;

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

    // Net spacing
    public float MinimumNetSpacing = 4.0f;

    private float hacktime;

    // Use this for initialization
    void Start() {
        // Initialize
        obstaclesInView = new ArrayList();
        enemiesInView = new ArrayList();
        balloonsInView = new ArrayList();
        // Get parent
        poacher = GetComponentInParent<PoacherController>();
        // Initialize
        timeSinceObstacleRedirect = float.MaxValue;
        timeSinceEnemyRedirect = float.MaxValue;
        // Hack
        hacktime = 0.0f;
    }

    // Update time variables
    void Update() {
        // Paused
        if (isPaused) return;
        // State
        timeSinceObstacleRedirect += Time.deltaTime;
        timeSinceEnemyRedirect += Time.deltaTime;
        // How many obstacles
        hacktime += Time.deltaTime;
    }

    // Keep track of things entering
    void OnTriggerEnter2D(Collider2D other) {
        if (hacktime < 1.0f) return; // hack
        if (other.gameObject.tag == "Obstacle" || other.gameObject.tag == "Water") {
            obstaclesInView.Add(other.gameObject);
            // Signal new direction (reverse)
            poacher.NewObjectiveSuggestion(AwayFromOverlappedObstacles());
            timeSinceObstacleRedirect = 0.0f;
        }
        else if (other.gameObject.tag == "Enemy") {
            enemiesInView.Add(other.gameObject);
            // Signal new direction (away from)
            poacher.NewObjectiveSuggestion((Vector2)(transform.position - other.gameObject.transform.position));
            timeSinceEnemyRedirect = 0.0f;
        }
        else if (other.gameObject.tag == "Balloon") {
            balloonsInView.Add(other.gameObject);
        }
    }

    // And exiting
    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Obstacle" || other.gameObject.tag == "Water") {
            obstaclesInView.Remove(other.gameObject);
        }
        else if (other.gameObject.tag == "Enemy") {
            enemiesInView.Remove(other.gameObject);
        }
        else if (other.gameObject.tag == "Balloon") {
            balloonsInView.Remove(other.gameObject);
        }
    }

    // Indicates whether or not it is an okay location to lay a trap.
    public bool CanLayTrap() {
        if (obstaclesInView.Count == 0 && enemiesInView.Count == 0 && balloonsInView.Count == 0) {
            // Must also check with poacher AI about net locations.
            foreach (Vector3 netLocation in PoacherAI.AI.GetNetLocations()) {
                // If too close to a net, cannot lay a new one.
                if (Vector3.Distance(poacher.transform.position, netLocation) < MinimumNetSpacing) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////
    // Calculates distance away from all overlapped obstacles
    public Vector2 AwayFromOverlappedObstacles() {
        Vector2 awayFromObstacles = Vector2.zero;
        foreach (GameObject obstacle in obstaclesInView) {
            awayFromObstacles += ((Vector2)(transform.position - obstacle.transform.position)).normalized;
        }
        return awayFromObstacles;
    }

    // Time since last redirect forced by obstacle collision
    public float TimeSinceObstacleRedirect() {
        return timeSinceObstacleRedirect;
    }

    // Time since last redirect forced by enemy collision
    public float TimeSinceEnemyRedirect() {
        return timeSinceEnemyRedirect;
    }

    
}
