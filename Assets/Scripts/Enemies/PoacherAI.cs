﻿using UnityEngine;
using System.Collections;

public class PoacherAI : MonoBehaviour, IPausable {

    // Playable area limits
    public Vector2 PlayableAreaLimits;
    // Difficulty
    public int DifficultyLevel;
    // Behavior tuning
    public float IdleInterval;
    public float NetInterval;
    public float RedirectInterval;
    public float JeepInterval;

    public GameObject jeep;

    // Singleton
    public static PoacherAI AI;

    // Keep a list of all poachers
    private ArrayList poachers;

    // For updating behavior goals
    private float timeSinceIdle;
    private float timeSinceNet;
    private float timeSinceRedirect;
    private float timeSinceJeep;
    private int nextPoacher;

    // Adjusted for difficult
    private float adjustedIdleInterval;
    private float adjustedNetInterval;
    private float adjustedRedirectInterval;

    // Store net locations
    private ArrayList netLocations;

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

    // Use this for initialization
    void Awake() {
        // Singleton (not persistent)
        if (AI != null) {
            DifficultyLevel = AI.DifficultyLevel + 1;
            GameObject.Destroy(AI);
        }
        else {
            DifficultyLevel = GameController.GC.difficulty;
        }
        AI = this;
        Debug.Log("Beginning level with difficulty: " + DifficultyLevel);
        // Adjust parameters for difficulty
        AdjustParametersForDifficulty();
        // Initialize poachers array
        poachers = new ArrayList();
        // Intialize net locations
        netLocations = new ArrayList();
        // Initialize state randomly (so they aren't synced)
        timeSinceIdle = Random.Range(0.0f, adjustedIdleInterval);
        timeSinceNet = Random.Range(0.0f, adjustedNetInterval);
        timeSinceRedirect = Random.Range(0.0f, adjustedRedirectInterval);
        timeSinceJeep = 0.0f;
        nextPoacher = 0;
    }

    // Register
    void Start() {
        GameController.GC.RegisterPausable(this);
    }

    // Update is called once per frame
    void Update() {
        // Pause interface
        if (isPaused) return;
        // If there are no poachers, nothing to do.
        if (poachers.Count == 0) return;
        // Idle every so often just to spice things up
        if (timeSinceIdle > adjustedIdleInterval) {
            int index = (nextPoacher % poachers.Count);
            ((PoacherController)poachers[index]).SetIdle();
            timeSinceIdle = 0.0f;
            nextPoacher++;
        }
        // Redirect every so often just to spice things up
        if (timeSinceRedirect > adjustedRedirectInterval) {
            int index = (nextPoacher % poachers.Count);
            ((PoacherController)poachers[index]).SetObjectiveDirection(Random.insideUnitCircle);
            ((PoacherController)poachers[index]).SetMoving();
            timeSinceRedirect = 0.0f;
            nextPoacher++;
        }
        // Tell the next poacher to deploy a trap
        if (timeSinceNet > adjustedNetInterval) {
            int index = (nextPoacher % poachers.Count);
            ((PoacherController)poachers[index]).SetLookingToTrap();
            timeSinceNet = 0.0f;
            nextPoacher++;
        }
        // Deploy a jeep if it is time.
        if (timeSinceJeep > JeepInterval) {
            Vector2 pos = 60.0f * Random.insideUnitCircle.normalized;
            Instantiate(jeep, (Vector3)pos, Quaternion.identity);
            timeSinceJeep = 0.0f;
        }
        // Update timers
        timeSinceIdle += Time.deltaTime;
        timeSinceNet += Time.deltaTime;
        timeSinceRedirect += Time.deltaTime;
        timeSinceJeep += Time.deltaTime;
    }

    // Request a new direction
    public Vector2 RequestNewDirection(Vector2 suggestion) {
        // Don't want this
        if (suggestion == Vector2.zero) return Random.insideUnitCircle;
        // Perturb suggestion
        suggestion = suggestion.normalized + 0.8f * Random.insideUnitCircle.normalized;
        // Return normalized
        return suggestion;
    }

    // Indicate that rhino is caught
    public void RhinoIsCaught() {
        foreach (PoacherController poacher in poachers) {
            poacher.SetRhinoCaught(true);
        }
    }

    // Register poachers
    public void RegisterPoacher(PoacherController poacher) {
        poachers.Add(poacher);
    }

    // Get list of poachers
    public ArrayList AllPoachers() {
        return poachers;
    }

    // Get net locations
    public ArrayList GetNetLocations() {
        return netLocations;
    }

    // Add a net location
    public void AddNetLocation(Vector3 location) {
        netLocations.Add(location);
    }

    // Adjust parameters for difficulty
    private void AdjustParametersForDifficulty() {
        // No change made to idle interval
        adjustedIdleInterval = IdleInterval / (float)DifficultyLevel;
        // Redirects scale with number of poachers
        adjustedRedirectInterval = RedirectInterval / (float)DifficultyLevel;
        // Net laying gets easier
        adjustedNetInterval = NetInterval + 3 * DifficultyLevel;

    }
}
