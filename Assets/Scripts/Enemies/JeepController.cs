﻿using UnityEngine;
using System.Collections;

public class JeepController : MonoBehaviour, IPausable {

    // Speed of jeep
    public int Speed;
    public float beepDistance;

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; animator.enabled = false; }
    public void Unpause() { isPaused = false; animator.enabled = true; }

    // Direction
    private Vector2 direction;
    private float angle;

    // For beeping
    Vector3 rhinoPos;
    bool needToBeep;

    // Animator
    Animator animator;

	// Use this for initialization
	void Start () {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Animator
        animator = GetComponent<Animator>();
	    // Choose a direction
        GameObject rhino = GameObject.FindGameObjectWithTag("Player");
        direction = new Vector2(rhino.transform.position.x - transform.position.x, rhino.transform.position.y - transform.position.y);
        direction.Normalize();
        angle = Vector2.Angle(Vector2.right, -direction.normalized);
        angle = direction.y > 0.0f ? -angle : angle;
        // Get the rhino
        rhinoPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        needToBeep = true;
	}

    // Update
    void Update() {
        if (isPaused) return;
        // Play sound if in screen.
        float distance = Vector2.Distance((Vector2)Camera.main.transform.position, rigidbody2D.position);
        float vol = distance < 32.0f ? 1.0f : Mathf.Exp((32.0f - distance) / 16.0f);
        AudioMaster.AM.JeepEngineSoundVolume(vol);
        // Beep when close to rhino
        if (needToBeep) {
            if (Vector3.Distance(rigidbody2D.position, rhinoPos) < beepDistance) {
                AudioMaster.AM.PlayJeepBeep();
                needToBeep = false;
            }
        }
        // Destroy
        if (rigidbody2D.position.magnitude > 100.0f) {
            AudioMaster.AM.JeepEngineSoundOff();
            Destroy(gameObject);
            return;
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        // Pause check
        if (isPaused) return;
        // Move
        rigidbody2D.MovePosition(rigidbody2D.position + 0.1f * Speed * Time.fixedDeltaTime * direction);
        rigidbody2D.MoveRotation(angle);
	}
}
