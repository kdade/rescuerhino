﻿using UnityEngine;
using System.Collections;

public class PoacherController : MonoBehaviour, IPausable {

    // Holds the net
    public GameObject NetHolder;
    // Movement parameters
    public float TurnDecay = 0.6f;
    public int Speed = 80;
    public float EnemyAvoidanceWeight = 1.0f;
    public float MaxIdleTime = 4.0f;
    public float CrouchDuration = 2.0f;

    //////////////// Internal stuff ////////////////////////////////////////////////

    // States
    private enum PoacherState {
        Moving,
        Idle,
        Trapping,
    };

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; PauseAnimations(); }
    public void Unpause() { isPaused = false; UnpauseAnimations(); }

    // Variables
    private PoacherState state;
    private Vector2 currentDirection;
    private float currentAngle;
    private Vector2 desiredDirection;
    // Behavior variables
    private Vector2 objectiveDirection;
    private float timeSinceLastTrap;
    private float elapsedTime;
    private float idleTimeHorizon;
    private bool lookingToTrap;
    // Animator
    private Animator animator;
    // Vision collider reference
    private VisionColliderScript visionCollider;
    // Obstacle collider reference
    private ObstacleColliderScript obstacleCollider;
    // And a reference to the rhino for chasing
    private GameObject rhino;
    private bool caughtRhino;
    private bool trapHasLeaves;
    // This is set if they are crouching
    private bool isCrouching;
    private float elapsedCrouchTime;

    // Use this for initialization
    void Start() {
        // Register with game controller
        GameController.GC.RegisterPausable(this);
        // Get animator
        animator = GetComponent<Animator>();
        // Get vision collider
        visionCollider = GetComponentInChildren<VisionColliderScript>();
        // Get obstacle collider
        obstacleCollider = GetComponentInChildren<ObstacleColliderScript>();
        // Register with AI
        PoacherAI.AI.RegisterPoacher(this);
        // Get a reference to the rhino
        rhino = GameObject.FindGameObjectWithTag("Player");
        caughtRhino = false;
        trapHasLeaves = true;
        // Intialize behavior values
        objectiveDirection = Random.insideUnitCircle.normalized; // Random initialization
        timeSinceLastTrap = 0.0f;
        elapsedTime = 0.0f;
        elapsedCrouchTime = 0.0f;
        lookingToTrap = false;
        // Initialize values
        currentDirection = -Vector2.right;
        currentAngle = 0.0f;
        desiredDirection = Vector2.zero;
        // Idle to begin with
        SetIdle();
    }

    // Update is called once per frame
    void Update() {
        // Check for pause
        if (isPaused) return;
        // Check if done crouching
        if (isCrouching && elapsedCrouchTime > CrouchDuration) {
            isCrouching = false;
            animator.SetBool("Crouching", false);
        }
        // Compute the behavior
        UpdateBehavior();
        // Update the direction
        UpdateDirection();
        // Keep track of time
        timeSinceLastTrap += Time.deltaTime;
        elapsedTime += Time.deltaTime;
        elapsedCrouchTime += Time.deltaTime;
    }

    // Physics update
    void FixedUpdate() {
        // Check for pause
        if (isPaused) return;
        // Move the poacher
        Move();
    }

    // Tell this object it needs to seek an objective in the new direction
    public void NewObjectiveSuggestion(Vector2 suggestion) {
        objectiveDirection = PoacherAI.AI.RequestNewDirection(suggestion).normalized;
    }

    // New objective
    public void SetObjectiveDirection(Vector2 objective) {
        if (RedirectWasTooRecent()) return;
        objectiveDirection = objective.normalized;
    }

    // Tell this poacher to idle
    public void SetIdle() {
        state = PoacherState.Idle;
        animator.SetBool("Running", false);
        elapsedTime = 0.0f;
        idleTimeHorizon = 1.0f + Random.Range(0.0f, Mathf.Max(0.0f, MaxIdleTime - 1.0f));
    }

    // Tell this poacher to move
    public void SetMoving() {
        if (!caughtRhino) {
            state = PoacherState.Moving;
            animator.SetBool("Running", true);
        }
    }

    // Tell this poacher to crouch
    public void SetCrouching() {
        animator.SetBool("Crouching", true);
        isCrouching = true;
        elapsedCrouchTime = 0.0f;
    }

    // Catch the rhino
    public bool CatchRhino() {
        // Only catch the rhino if not crouching
        if (!isCrouching) {
            trapHasLeaves = false;
            SetTrapping();
            PoacherAI.AI.RhinoIsCaught();
            return true;
        }
        else {
            return false;
        }
    }

    // Set caught to true
    public void SetRhinoCaught(bool isCaught) {
        caughtRhino = isCaught;
    }

    // Tell this poacher to lay a trap in the next good location
    public void SetLookingToTrap() {
        lookingToTrap = true;
    }

    // Tell this poacher to lay a trap
    public void SetTrapping() {
        // Only if we have passed enough time since last time.
        if (!caughtRhino) {
            // Start trapping stuff
            state = PoacherState.Trapping;
            animator.SetBool("Running", false);
            // Sound (only if on screen)
            if (Camera.main.GetComponent<CameraController>().IsObjectVisible(gameObject)) {
                AudioMaster.AM.PlayPoacherThrow();
                AudioMaster.AM.PlayNetSwish();
            }
        }
    }

    ///////////// Helper Methods /////////////////////

    // Update behavior each frame
    private void UpdateBehavior() {
        // Depends on what he is doing now
        switch (state) {
            case PoacherState.Idle:
                if (!caughtRhino && elapsedTime > idleTimeHorizon) {
                    SetMoving();
                }
                break;
            case PoacherState.Moving:
                MovingLogic();
                break;
            case PoacherState.Trapping:
                TrappingLogic();
                break;
        }
    }

    // Update direction each frame
    private void UpdateDirection() {
        // Update direction for moving
        if (state == PoacherState.Moving && !isCrouching) {
            currentDirection = TurnDecay * currentDirection + (1.0f - TurnDecay) * desiredDirection.normalized;
        }
        // Update angle
        if (currentDirection != Vector2.zero) {
            currentAngle = -Vector2.Angle(Vector2.right, -currentDirection.normalized);
            currentAngle = currentDirection.y > 0.0f ? currentAngle : -currentAngle;
        }
    }

    // Move the poacher
    private void Move() {
        // Set movement and rotation
        if (state == PoacherState.Moving && !isCrouching) {
            rigidbody2D.MovePosition(transform.position + 0.1f * Speed * Time.fixedDeltaTime * new Vector3(currentDirection.x, currentDirection.y, 0.0f));
        }
        rigidbody2D.MoveRotation(currentAngle);
    }


    ///////////// Logic for flocking behavior /////////////////

    // Logic while moving
    private void MovingLogic() {
        // If the rhino is caught, we are done
        if (caughtRhino) {
            SetIdle();
            return;
        }
        // If leaving the playing field, must head back towards center
        if (Mathf.Abs(rigidbody2D.position.x) > PoacherAI.AI.PlayableAreaLimits.x || Mathf.Abs(rigidbody2D.position.y) > PoacherAI.AI.PlayableAreaLimits.y) {
            desiredDirection = -rigidbody2D.position;
            return;
        }
        // If overlapping obstacle, must move away from it
        Vector2 awayFromObstacles = obstacleCollider.AwayFromOverlappedObstacles();
        if (awayFromObstacles != Vector2.zero) {
            desiredDirection = awayFromObstacles.normalized;
            return;
        }
        // If redirect was recent, must stick with it
        if (RedirectWasTooRecent()) {
            desiredDirection = objectiveDirection;
            return;
        }
        // If rhino is visible, chase
        if (visionCollider.RhinoInView()) {
            desiredDirection = (rhino.rigidbody2D.position - rigidbody2D.position).normalized;
        }
        // Otherwise, stick with objective
        else {
            desiredDirection = objectiveDirection;
        }
        // Get direction away from visible poachers
        Vector2 awayFromEnemies = Vector2.zero;
        int numEnemies = 0;
        foreach (GameObject enemy in visionCollider.EnemiesInView()) {
            numEnemies++;
            Vector2 enemyPos = (Vector2)(enemy.transform.position);
            Vector2 away = rigidbody2D.position - enemyPos;
            awayFromEnemies += away.normalized / Mathf.Clamp(away.magnitude, 0.1f, float.MaxValue);
        }
        if (numEnemies > 0) awayFromEnemies /= (float)numEnemies;
        // Now we have a normalized objective direction, and a scaled direction away from enemies with magnitude at most 10.
        desiredDirection += EnemyAvoidanceWeight * awayFromEnemies;
        // If the magnitude is too small, then go to idle
        if (desiredDirection.magnitude < 0.1f) {
            SetIdle();
        }
        // If looking to trap, and in a good location, go to trapping state
        if (lookingToTrap && obstacleCollider.CanLayTrap()) {
            SetTrapping();
        }
    }

    // Logic for trapping state
    private void TrappingLogic() {
        // Direction for the net
        Quaternion netRotation;
        // Throw the net at the rhino
        if (caughtRhino) {
            currentDirection = new Vector2(rhino.transform.position.x - rigidbody2D.transform.position.x, rhino.transform.position.y - rigidbody2D.transform.position.y);
            currentAngle = -Vector2.Angle(Vector2.right, -currentDirection.normalized);
            currentAngle = currentDirection.y > 0.0f ? currentAngle : -currentAngle;
            netRotation = Quaternion.Euler(0.0f, 0.0f, currentAngle);
        }
        // Otherwise, this is just a regular net deployment
        else {
            // Snap to cardinal direction
            int facing = (int)Mathf.Floor(((rigidbody2D.transform.rotation.eulerAngles.z - 45.0f) % 360.0f) / 90.0f);
            facing = facing < 0 ? 0 : facing; // Necessary hack fix.
            switch (facing) {
                case 0: currentDirection = -Vector2.right; //desiredDirection = -Vector2.right;
                    break;
                case 1: currentDirection = -Vector2.up; //desiredDirection = -Vector2.up;
                    break;
                case 2: currentDirection = Vector2.right; //desiredDirection = Vector2.right;
                    break;
                case 3: currentDirection = Vector2.up; //desiredDirection = Vector2.up;
                    break;
            }
            // Instantiate net
            netRotation = Quaternion.Euler(0.0f, 0.0f, 90.0f * facing);
        }

        // Don't play running animation
        animator.SetBool("Running", false);
        animator.SetTrigger("ThrowNet");
        // Make the net
        GameObject net = Instantiate(NetHolder, transform.position, netRotation) as GameObject;
        NetHolderScript netScript = net.GetComponent<NetHolderScript>();
        netScript.SetLeavesEnabled(trapHasLeaves);
        // If this is a catch, set the net moving
        if (!trapHasLeaves) {
            netScript.MoveTowardsRhino(rhino.transform.position);
        }
        // Register this net with poacher AI.
        PoacherAI.AI.AddNetLocation(netScript.Leaves.transform.position);
        // Reset state
        timeSinceLastTrap = 0.0f;
        // Leave state
        SetIdle(); // ERROR?
        // Turn off looking to trap
        lookingToTrap = false;
    }

    // Pauses all animations
    void PauseAnimations() {
        animator.enabled = false;
    }

    // Unpauses all animations
    void UnpauseAnimations() {
        animator.enabled = true;
    }

    // Recent redirect, this is a heuristic constant
    bool RedirectWasTooRecent() {
        return obstacleCollider.TimeSinceObstacleRedirect() < 0.8f;
    }
}
