﻿using UnityEngine;
using System.Collections;

public class VisionColliderScript : MonoBehaviour {
    // Vision collider is the larger collider.
    // Keep track of enemies, and rhino.

    // All enemies in view
    private ArrayList enemiesInView;
    // Rhino in view
    private bool rhinoInView;

    //Hack
    private float hacktime = 0.0f;

	// Use this for initialization
	void Start () {
	    // Initialize
        enemiesInView = new ArrayList();
        rhinoInView = false;
        hacktime = 0.0f;
	}

    void Update() {
        hacktime += Time.deltaTime;
    }
	
    // Keep track of things entering
    void OnTriggerEnter2D(Collider2D other) {
        if (hacktime < 1.0f) return; // hack
        if (other.gameObject.tag == "Enemy") {
            enemiesInView.Add(other.gameObject);
        }
        else if (other.gameObject.tag == "Player") {
            rhinoInView = true;
        }
    }

    // And exiting
    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Enemy") {
            enemiesInView.Remove(other.gameObject);
        }
        else if (other.gameObject.tag == "Player") {
            rhinoInView = false;
        }
    }

    // Query the enemies in view
    public ArrayList EnemiesInView() {
        return enemiesInView;
    }

    // Query rhino in view
    public bool RhinoInView() {
        return rhinoInView;
    }
}
