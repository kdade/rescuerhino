﻿using UnityEngine;
using System.Collections;

public class BalloonScript : MonoBehaviour, IPausable {

    // Balloon sprites
    public Sprite RedSprite;
    public Sprite PurpleSprite;
    public Sprite RedPop;
    public Sprite PurplePop;
    public SpriteRenderer fragmentsSpriteRenderer;
    // Bird flock object
    public GameObject BirdFlockObject;
    public GameObject PerfectObject;
    public GameObject PointsObject;
    // Hit radius
    public float PerfectHitRadius;
    // Duration of balloon pop
    public float popDuration;
    // Poacher disable radius
    public float PoacherDisableRadius;

    // Instance vars
    private Animator animator;
	private SpriteRenderer spriteRenderer;
    // State
	private bool popped;
	private float popElapsedTime;

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

    // Initialize
	void Start() {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Get animator
        animator = GetComponent<Animator>();
        // Get renderer
        spriteRenderer = renderer as SpriteRenderer;
        // Initialize
		popped = false;
        // Randomly set the sprite renderer to choose between two sprites
        ChooseSprite();
	}

    // Remove once popped
	public void Update() {
        // Check for pause
        if (isPaused) return;

        // Destroy if done popping.
		if(popped && popElapsedTime > popDuration) {
			Destroy (gameObject);
		}

        // State
        popElapsedTime += Time.deltaTime;
	}

    // Take whatever action necessary to "Pop" this balloon
	public void PopBalloon(Vector3 rhinoPos) {
        // Check for perfect hit
        if (Vector3.Distance(rhinoPos, transform.position) < PerfectHitRadius) {
            PerfectHit();
        }
        // Otherwise it is a normal hit
        else {
            NormalHit();
        }
        // Disable nearby poachers and dispatch angry bird flocks...
        foreach (PoacherController poacher in PoacherAI.AI.AllPoachers()) {
            if (Vector3.Distance(transform.position, poacher.transform.position) < PoacherDisableRadius) {
                // Disable the poacher
                poacher.SetCrouching();
                // Dispatch bird flock
                GameObject flock = Instantiate(BirdFlockObject, transform.position, transform.rotation) as GameObject;
                flock.GetComponent<BirdFlockController>().AttackTarget(poacher.transform.position);
            }
        }
        // Instantiate points object
        Instantiate(PointsObject, transform.position, transform.rotation);
        // Update state
		popped = true;
		popElapsedTime = 0.0f;
	}

    // Perfect hit!
    private void PerfectHit() {
        // Animate
        animator.SetTrigger("Pop");
        // Play sound
        AudioMaster.AM.PlayBalloonPerfectPop();
        // Instantiate perfect image
        Instantiate(PerfectObject, transform.position, transform.rotation);
        // Increment spree
        GameController.GC.RecordPerfectPop();
    }

    // Normal hit :(
    private void NormalHit() {
        // Animate
        animator.SetTrigger("Pop");
        // Play sound
        AudioMaster.AM.PlayBalloonNormalPop();
        // Reset spree
        GameController.GC.RecordNormalPop();
    }

    // Randomly set sprite renderer to choose between sprites
    private void ChooseSprite() {
        if (Random.Range(0.0f, 1.0f) < 0.5f) {
            spriteRenderer.sprite = RedSprite;
            fragmentsSpriteRenderer.sprite = RedPop;
        }
        else {
            spriteRenderer.sprite = PurpleSprite;
            fragmentsSpriteRenderer.sprite = PurplePop;
        }
    }
}
