﻿using UnityEngine;
using System.Collections;

public class PointsScript : MonoBehaviour, IPausable {

    // Control params
    public Sprite Points100;
    public Sprite Points150;
    public Sprite Points200;
    public float yOffset;
    public float duration;
    public float speed;
    public float gravityDecay;

    // Renderer
    private SpriteRenderer srenderer;
    // Random start direction
    Vector2 direction;
    // State
    float elapsedTime;


    //Pausable interface
    bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

	// Use this for initialization
	void Start () {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Get renderer
        srenderer = GetComponent<SpriteRenderer>();
        // Ask the game controller what the spree is
        int spree = GameController.GC.GetPerfectSpree();
        switch (spree) {
            case 0: // fall through
            case 1: srenderer.sprite = Points100; break;
            case 2: srenderer.sprite = Points150; break;
            default: srenderer.sprite = Points200; break;
        }
        // Offset in Y
        transform.position += new Vector3(0.0f, yOffset, 0.0f);
        // Direction
        direction = new Vector2(Random.Range(-1.4f, 1.4f), 1.0f).normalized;
        // State
        elapsedTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        // Return on pause
        if (isPaused) return;
        // Destroy if overdue
        if (elapsedTime > duration) {
            Destroy(gameObject);
            return;
        }
        // Update direction
        direction = (gravityDecay * direction - (1.0f - gravityDecay) * Vector2.up).normalized;
        // Move
        transform.position += (Vector3)direction * speed * Time.deltaTime;
        // Alpha fade
        float scale = Mathf.Sin(1.0f * Mathf.PI * elapsedTime / duration);
        srenderer.color = new Color(srenderer.color.r, srenderer.color.g, srenderer.color.b, scale);
	    // State
        elapsedTime += Time.deltaTime;
	}
}
