﻿using UnityEngine;
using System.Collections;

public class BirdFlockController : MonoBehaviour, IPausable {

    // Speed of flight
    public float Speed;
    // Attack duration
    public float AttackDuration;

    // Pausable interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; PauseAnimations(); }
    public void Unpause() { isPaused = false; UnpauseAnimations(); }

    // Target
    private Vector3 attackTarget;
    private float elapsedTime;
    private Animator[] birdAnimators;
    private Vector2 dispersalDirection;

    // Sound
    bool needToCaw;

    // Use this for initialization
    void Awake() {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Intialize
        elapsedTime = 0.0f;
        dispersalDirection = RandomThroughClosestEdge();
        // Initialize animators
        birdAnimators = GetComponentsInChildren<Animator>();
        birdAnimators[0].SetBool("Swarm1", true);
        birdAnimators[1].SetBool("Swarm2", true);
        birdAnimators[2].SetBool("Swarm3", true);
        birdAnimators[0].speed = Random.Range(0.8f, 1.2f);
        birdAnimators[1].speed = Random.Range(0.8f, 1.2f);
        birdAnimators[2].speed = Random.Range(0.8f, 1.2f);
        needToCaw = true;
    }

    // Update is called once per frame
    void Update() {
        // Pausable
        if (isPaused) return;
        // Play sound if in screen.
        float distance = Vector2.Distance((Vector2)Camera.main.transform.position, transform.position);
        float vol = 0.5f * Mathf.Exp(-distance / 20.0f);
        AudioMaster.AM.BirdFlockingSoundVolume(vol);
        // Still actively disabling poachers
        if (elapsedTime < AttackDuration) {
            // Move towards target
            Vector3 towardsTarget = attackTarget - transform.position;
            if (towardsTarget.magnitude > 0.1f) {
                transform.position += towardsTarget.normalized * Time.deltaTime * Speed;
            }
            else if (needToCaw) {
                AudioMaster.AM.PlayBirdCaw();
                needToCaw = false;
            }
        }
        // Otherwise flying off of the screen
        else {
            // Disperse
            transform.position += (Vector3)dispersalDirection * Time.deltaTime * Speed;
            // Destroy if all birds are out
            if (transform.position.magnitude > 70.0f) {
                Destroy(gameObject);
                return;
            }
        }
        // Update time
        elapsedTime += Time.deltaTime;
    }

    // Set all the relevant parameters
    public void AttackTarget(Vector3 target) {
        attackTarget = target;
    }

    // Choose dispersion direction
    private Vector2 RandomThroughClosestEdge() {
        Vector2 outDir = Vector2.zero;
        // Leave horizontally
        if (Mathf.Abs(transform.position.x) / 4.0f > Mathf.Abs(transform.position.y) / 3.0f) {
            outDir.x = Mathf.Sign(transform.position.x);
            outDir.y = Random.Range(-0.5f, 0.5f);
        }
        // Leave vertically
        else {
            outDir.y = Mathf.Sign(transform.position.y);
            outDir.x = Random.Range(-0.5f, 0.5f);
        }
        // Done
        return outDir.normalized;
    }

    // Pause animations
    private void PauseAnimations() {
        foreach (Animator animator in birdAnimators) animator.enabled = false;
    }

    // Unpause
    private void UnpauseAnimations() {
        foreach (Animator animator in birdAnimators) animator.enabled = true;
    }
}
