﻿using UnityEngine;
using System.Collections;

public class PerfectScript : MonoBehaviour, IPausable {

    // Control params
    public float duration;
    public float maxScale;
    public Color color1;
    public Color color2;
    public Color color3;

    // Reference
    private SpriteRenderer srenderer;
    // State
    private float elapsedTime;
    //Pausable interface
    bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

	// Use this for initialization
	void Start () {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Initialize
        srenderer = GetComponent<SpriteRenderer>();
        elapsedTime = 0.0f;
        // Intialize
        srenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

	}
	
	// Update is called once per frame
	void Update () {
	    // Do nothing if paused
        if (isPaused) return;
        // Destroy when time is up
        if (elapsedTime > duration) {
            Destroy(this);
            return;
        }
        // Animate scale
        float scale = Mathf.Sin(1.0f * Mathf.PI * elapsedTime / duration);
        transform.localScale = new Vector3(scale, scale, scale) * maxScale;
        // Animate color
        float scale1 = Mathf.Abs(Mathf.Sin(3.0f * Mathf.PI * elapsedTime / duration));
        float scale2 = Mathf.Abs(Mathf.Sin(3.0f * Mathf.PI * (elapsedTime - duration / 3.0f) / duration));
        float scale3 = Mathf.Abs(Mathf.Sin(3.0f * Mathf.PI * (elapsedTime - 2.0f*duration / 3.0f) / duration));
        Vector4 color = scale1 * color1 + scale2 * color2 + scale3 * color3;
        color = new Color(Mathf.Clamp01(color.x), Mathf.Clamp01(color.y), Mathf.Clamp01(color.z), scale);
        srenderer.color = color;
        // State
        elapsedTime += Time.deltaTime;
	}
}
