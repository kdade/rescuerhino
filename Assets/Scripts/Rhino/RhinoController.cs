﻿using UnityEngine;
using System.Collections;

public class RhinoController : MonoBehaviour, IGestureListener, IPausable {

    // Public references and tuning parameters
    public GestureInput Gesture;
    public int MinChargeVelocity = 100;
    public int MaxChargeVelocity = 200;
    public float TurnDecay = 0.95f;
    public float MoveDecay = 0.99f;
    public float MoveMapTuning = 1.5f;
    public int BounceBackSpeed = 30;
    public float BounceBackDuration = 0.8f;
    // Animation duration parameters
    public float StunAnimationDuration = 2.0f;
    public float PopAnimationDuration = 0.8f;
    // Water sprite
    public GameObject waterObject;
    public float WaterSlowdown = 0.5f;

    // State machines
    private enum MoveState {
        Idle,
        Moving,
        Stunned,
        Trapped,
        Pancaked
    };
    private enum PopState {
        NoAction,
        Popping,
        Popped
    }

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; PauseAnimations(); }
    public void Unpause() { isPaused = false; UnpauseAnimations(); }

    // Instance vars ---------------------------------------- //
    // Gesture copies
    private Vector2 gestureDirection;
    // Direction interpolation state vars
    private float currentAngle;
    private Vector2 currentDirection;
    private Vector2 desiredDirection;
    // Mapped movement
    private Vector2 mappedMovement;
    private bool isDecaying;
    private float moveScale;
    // Speed limits
    private int minVelocity = 60;
    private int maxVelocity = 200;
    // Animation states
    private MoveState moveState;
    private float moveStateElapsedTime;
    private PopState popState;
    private float popStateElapsedTime;
    // List of balloons
    private ArrayList overlappingBalloons;
    // Animator
    private Animator animator;    
    // The horn collider
    private HornColliderScript hornCollider;
    // This is the trap we hit
    private Vector3 trapPosition;
    // Whether or not we are in water
    private bool inWater;
    private int waterOverlaps;
    private int deepWaterOverlaps;
    // Audio hack
    private float squealDelay = 0.3f;
    bool done = false;

    // Use this for initialization
    void Start() {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        // Register with gesture listener
        Gesture.RegisterGestureListener(this);
        // Get Animator
        animator = GetComponent<Animator>();
        // Get horn collider
        hornCollider = GetComponentInChildren<HornColliderScript>();
        // Initialize the ArrayList
        overlappingBalloons = new ArrayList();
        // Initialize
        currentAngle = 0.0f;
        currentDirection = -Vector2.right;
        desiredDirection = -Vector2.right;
        mappedMovement = -Vector2.right * minVelocity;
        isDecaying = true;
        moveScale = minVelocity;
        inWater = false;
        waterOverlaps = 0;
        deepWaterOverlaps = 0;
        // Initialize state
        moveState = MoveState.Idle;
        moveStateElapsedTime = 0.0f;
        popState = PopState.NoAction;
        popStateElapsedTime = 0.0f;
    }

    // Update is called once per frame
    void Update() {
        // Check for pause
        if (isPaused) return;
        // Dead
        if (!done && moveState == MoveState.Pancaked && moveStateElapsedTime > 3.0f) {
            done = true;
            GameController.GC.GameOver();
        }
        // Calculate new direction
        UpdateDirection();
        // Handle animation
        Animate();
    }

    // Handle movement and collisions
    void FixedUpdate() {
        // Check for pause
        if (isPaused) return;

        // Map the movement
        Move();
    }

    // Called when a tap is detected
    public void OnTapEvent(Vector2 tapLocation) {
        // Check for pause
        if (isPaused) return;
        // Tap logic
        BeginPop();
    }

    // Called when a flick is detected
    public void OnFlickEvent(Vector2 flickDirection, float flickMagnitude) {
        // Check for pause
        if (isPaused) return;
        // Flick logic
        if (flickMagnitude > 1e-2) {
            desiredDirection = flickDirection * flickMagnitude;
        }
        isDecaying = false;
        BeginMoving();
    }

    // Called when a hold is detected
    public void OnHoldEvent(Vector2 holdLocation) {
        // Check for pause
        if (isPaused) return;
        // Hold logic
        GameController.GC.PauseGame();
    }

    // Collision with balloons, enemies, obstacles, etc.
    void OnTriggerEnter2D(Collider2D other) {
        // Deal with hit obstacles
        if (other.gameObject.tag == "Obstacle") {
            // Trigger stunned animation
            BeginStunned();
        }
        // If a poacher is hit
        else if (other.gameObject.tag == "Enemy") {
            // Tell poacher to run trap animation, if it starts, death
            if (other.gameObject.GetComponent<PoacherController>().CatchRhino()) {
                // TRAPPED
                BeginPancaked();
            }
            else {
                BeginStunned();
            }
        }
        // If jeep is hit
        else if (other.gameObject.tag == "Jeep") {      
            PoacherAI.AI.RhinoIsCaught();
            squealDelay = 0.0f;
            BeginPancaked();
        }
        // If trap is hit
        else if (other.gameObject.tag == "Net") {
            PoacherAI.AI.RhinoIsCaught();
            // Set leaves exploding
            other.gameObject.GetComponentInParent<NetHolderScript>().StartLeafExplosion();
            other.gameObject.GetComponentInParent<NetHolderScript>().SetLeavesEnabled(false);
            // Start animation
            BeginTrapped(other.gameObject);
        }
        // Entering water
        else if (other.gameObject.tag == "Water") {
            inWater = true;
            waterOverlaps++; 
            BeginMoving();
        }
        // Deep water
        else if (other.gameObject.tag == "DeepWater") {
            waterObject.SetActive(true);
            deepWaterOverlaps++;
        }
    }

    // For exiting water
    void OnTriggerExit2D(Collider2D other) {
        // Exit water
        if (other.gameObject.tag == "Water") {
            waterOverlaps--;
            if (waterOverlaps <= 0) inWater = false; ;
            BeginMoving();
        }
        // Deep water
        else if (other.gameObject.tag == "DeepWater") {
            deepWaterOverlaps--;
            if (deepWaterOverlaps <= 0) {
                waterObject.SetActive(false);
                deepWaterOverlaps = 0;
            }
        }
    }

    // When a balloon is entered
    public void OnBalloonEnter(GameObject balloon) {
        overlappingBalloons.Add(balloon.gameObject);
    }
    // When a balloon is exited
    public void OnBalloonExit(GameObject balloon) {
        overlappingBalloons.Remove(balloon.gameObject);
    } 

    ////////////////////////////// Helpers /////////////////////////////////////////////////

    // Updates the direction vector according to the interpolation parameters
    private void UpdateDirection() {
        // Otherwise, track towards the new heading
        currentDirection = TurnDecay * currentDirection + (1.0f - TurnDecay) * desiredDirection;
        if (currentDirection != Vector2.zero) {
            currentAngle = -Vector2.Angle(Vector2.right, -currentDirection.normalized);
            currentAngle = currentDirection.y > 0.0f ? currentAngle : -currentAngle;
        }
    }

    // Maps the movement to the acceptable range and moves the rhino.
    private void Move() {
        // Decay towards the minimum
        if (isDecaying) {
            moveScale = MoveDecay * moveScale + (1 - MoveDecay) * minVelocity;
        }
        // Otherwise assume there is a new value to update, begin decay phase
        else {
            // Map the movement through some "nonlinear" mapping
            moveScale = Mathf.Clamp(minVelocity + (maxVelocity - minVelocity) * Mathf.Pow(currentDirection.magnitude / MoveMapTuning, 1.5f), minVelocity, maxVelocity);
            if (currentDirection.magnitude > MoveMapTuning) {
                moveScale = maxVelocity;
            }
            else {
                moveScale = minVelocity;
            }
            // Begin decay phase
            isDecaying = true;
        }
        // Set the movement
        mappedMovement = 0.1f * moveScale * currentDirection.normalized;
        // Only move while running or swimming
        if (moveState == MoveState.Moving) {
            // Set movement and rotation
            rigidbody2D.MovePosition(transform.position + Time.fixedDeltaTime * new Vector3(mappedMovement.x, mappedMovement.y, 0.0f));
            rigidbody2D.MoveRotation(currentAngle);
        }
    }

    // Update animation state machines
    private void Animate() {
        // Update move state
        switch (moveState) {
            case MoveState.Idle:
                break;
            case MoveState.Moving:
                break;
            case MoveState.Stunned:
                if (moveStateElapsedTime >= StunAnimationDuration) {
                    moveState = MoveState.Idle;
                    animator.speed = inWater ? WaterSlowdown : 1.0f;
                }
                else if (moveStateElapsedTime < BounceBackDuration) {
                    // This moves him backwards from the collision a bit
                    rigidbody2D.MovePosition(transform.position + 0.1f * BounceBackSpeed * Time.deltaTime * new Vector3(-mappedMovement.normalized.x, -mappedMovement.normalized.y, 0.0f));
                }
                break;
            case MoveState.Trapped:
                // Move towards trap
                MoveTowardsTrap();
                break;
            case MoveState.Pancaked:
                animator.SetBool("Pancaked", true);
                break;
        }
        // Update popping state
        switch (popState) {
            case PopState.NoAction:
                break;
            case PopState.Popping:
                if (popStateElapsedTime >= PopAnimationDuration) {
                    popState = PopState.Popped;
                }
                break;
            case PopState.Popped:
                PopNearbyBalloons();
                popState = PopState.NoAction;
                animator.speed = inWater ? WaterSlowdown : 1.0f;
                break;
        }
        // Increment frame counts
        moveStateElapsedTime += Time.deltaTime;
        popStateElapsedTime += Time.deltaTime;
    }

    ////////////////////////////////////////////////////////////////////
    // State transitions

    // Play the pop animation
    void BeginPop() {
        // May only transition from certain move/pop state combos
        if (moveState == MoveState.Idle || moveState == MoveState.Moving) {
            if (popState == PopState.NoAction) {
                // Animation
                animator.SetTrigger("HeadToss");
                animator.speed = 1.0f;
                // Sound
                AudioMaster.AM.PlayRhinoGrunt();
                // State
                popState = PopState.Popping;
                popStateElapsedTime = 0.0f;
            }
        }
    }

    // Play the stunned animation
    void BeginStunned() {
        // May only transition from certain move/pop state combos
        if (moveState == MoveState.Idle || moveState == MoveState.Moving) {
            // Animator
            animator.SetTrigger("Collision");
            animator.SetBool("Running", false);
            animator.speed = 1.0f;
            // Audio
            AudioMaster.AM.PlayRhinoBonk();
            AudioMaster.AM.RhinoIdleSound();
            // State
            desiredDirection = Vector2.zero;
            moveState = MoveState.Stunned;
            moveStateElapsedTime = 0.0f;
            popState = PopState.NoAction;
            popStateElapsedTime = 0.0f;
        }
    }

    // Play the moving animation
    void BeginMoving() {
        // May only transition from certain move/pop state combos
        if (moveState == MoveState.Idle || moveState == MoveState.Moving) {
            // In water
            if (inWater) {
                // Animation
                //animator.SetBool("Running", false);
                animator.SetBool("Running", true);
                animator.speed = WaterSlowdown;
                // Audio
                AudioMaster.AM.RhinoSwimmingSound();
                // Speed
                minVelocity = (int)(MinChargeVelocity * WaterSlowdown);
                maxVelocity = (int)(MaxChargeVelocity * WaterSlowdown);
            }
            // Or on land
            else {
                // Animation
                animator.SetBool("Running", true);
                animator.speed = 1.0f;
                // Audio
                AudioMaster.AM.RhinoRunningSound();
                // Speed
                minVelocity = MinChargeVelocity;
                maxVelocity = MaxChargeVelocity;
            }
            
            // Set state
            moveState = MoveState.Moving;
            moveStateElapsedTime = 0.0f;
        }
    }

    // Begin falling into trap
    void BeginTrapped(GameObject trap) {
        trapPosition = trap.transform.position;
        // Animator
        animator.SetBool("Running", false);
        animator.SetBool("Pancaked", true);
        animator.speed = 1.0f;
        // Audio
        AudioMaster.AM.PlayRhinoSqueal(squealDelay);
        AudioMaster.AM.RhinoIdleSound();
        moveState = MoveState.Trapped;
        moveStateElapsedTime = 0.0f;
    }

    // Pancaked animation
    void BeginPancaked() {
        // Animator
		animator.SetBool("Running", false);
		animator.SetBool ("Pancaked", true);
        animator.speed = 1.0f;
        // Audio
        AudioMaster.AM.PlayRhinoSqueal(squealDelay);
        AudioMaster.AM.RhinoIdleSound();
        // State
        moveState = MoveState.Pancaked;
        moveStateElapsedTime = 0.0f;
    }

    // Pop all nearby balloons
    void PopNearbyBalloons() {
        // Check for nearby balloons to pop.
        bool hit = false;
        foreach (GameObject balloon in overlappingBalloons) {
            if (balloon != null) {
                balloon.GetComponent<BalloonScript>().PopBalloon(hornCollider.Position());
                hit = true;
            }
        }
        overlappingBalloons.Clear();
        // If no hit, end the spree
        if (!hit) {
            GameController.GC.RecordHeadbuttMiss();
        }
    }

    // Move towards the trap
    void MoveTowardsTrap() {
        Vector3 direction = (trapPosition - transform.position);
        if (direction.magnitude < 0.1f) {
            return;
        }
        else {
            transform.position += direction.normalized * Time.deltaTime * 10;
        }

    }

    // Pauses all animations
    void PauseAnimations() {
        animator.enabled = false;
    }

    // Unpauses all animations
    void UnpauseAnimations() {
        animator.enabled = true;
    }
}

