﻿using UnityEngine;
using System.Collections;

public class HornColliderScript : MonoBehaviour {

    // All balloons in view
    private RhinoController rhino;

    // Start
    void Start() {
        // Get the script off the parent
        rhino = GetComponentInParent<RhinoController>();
    }

    // Keep track of things entering
    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Balloon") {
            rhino.OnBalloonEnter(other.gameObject);
        }
    }

    // And exiting
    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Balloon") {
            rhino.OnBalloonExit(other.gameObject);   
        }
    }

    // Get location
    public Vector3 Position() {
        return transform.position;
    }
	
}
