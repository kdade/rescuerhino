﻿using UnityEngine;
using System.Collections;

public class ExplodingLeafScript : MonoBehaviour, IPausable {

    // Variables to control movement
    public float ExplodeStartRadius;
    public float ExplodeSpeed;
    public float ExplodeDuration;
    public float MoveDecay;

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

    // Movement variables
    private Vector3 initialDirection;
    private Vector3 currentDirection;
    private Quaternion rotationStep1;
    private Quaternion rotationStep2;
    private float duration;
    private float elapsedTime;

	// Use this for initialization
	void Start () {
	    // Pausable
        GameController.GC.RegisterPausable(this);
        // Randomize position
        transform.position +=(Vector3)Random.insideUnitCircle * ExplodeStartRadius;
        initialDirection = (Vector3)Random.insideUnitCircle.normalized;
        currentDirection = (Vector3)Random.insideUnitCircle.normalized;
        rotationStep1 = Random.rotation;
        rotationStep2 = Random.rotation;
        duration = Random.Range(0.75f * ExplodeDuration, ExplodeDuration);
        // Intialize
        elapsedTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
	    // Pause
        if (isPaused) return;
        // End
        if (elapsedTime > duration) {
            Destroy(gameObject);
            return;
        }
        // Move
        Move();
        // State
        elapsedTime += Time.deltaTime;
	}

    // Move leaf
    private void Move() {
        // desiredDirect
        // Update direction
        currentDirection = MoveDecay * currentDirection + (1 - MoveDecay) * initialDirection;
        // And move
        transform.rotation = rotationStep1 * transform.rotation;
        transform.rotation = rotationStep2 * transform.rotation;
        transform.position += currentDirection * ExplodeSpeed * Time.deltaTime;
    }
}
