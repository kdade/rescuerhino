﻿using UnityEngine;
using System.Collections;

public class NetHolderScript : MonoBehaviour, IPausable {

    public SpriteRenderer Net;
    public GameObject Leaves;
    public GameObject Leaf;
    public float DeploySpeed;
    public int ExplodeNumLeaves;

    // Pause Interface
    private bool isPaused = false;
    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

    // Rhino location
    Vector3 rhinoPosition;
    private float elapsedTime;

    // Set here to insure it is good to go on initalization
    bool moving = false;
    // Leaf explosion
    bool isExploding;

    // Start
    void Start() {
        // Register pausable
        GameController.GC.RegisterPausable(this);
        isExploding = false;
    }

    // Update
    void Update() {
        // Paused
        if (isPaused) return;
        // Moving
        if (moving && elapsedTime > 0.5f) {
            // Otherwise, move towards the target.
            Vector3 direction = rhinoPosition - transform.position;

            if (direction.magnitude < 2.7f) {
                moving = false;
                return;
            }
            transform.position += direction.normalized * Time.deltaTime * DeploySpeed;
        }
        // Time update
        elapsedTime += Time.deltaTime;
    }

    // Turn on the leaves
    public void SetLeavesEnabled(bool en) {
        Leaves.SetActive(en);
        // Disabling the leaves means the net is catching the rhino and should render on top
        if (!en) {
            Net.sortingLayerName = "Poacher";
        }
    }

    // Start moving towards the rhino.
    public void MoveTowardsRhino(Vector3 rhinoPos) {
        rhinoPosition = rhinoPos;
        moving = true;
        elapsedTime = 0.0f;
    }

    // Start capture animation
    public void StartLeafExplosion() {
        if (!isExploding) {
            isExploding = true;
            // Spawn leaves
            for (int i = 0; i < ExplodeNumLeaves; i++) {
                Instantiate(Leaf, Leaves.transform.position, Random.rotation);
            }
        }
    }
}
